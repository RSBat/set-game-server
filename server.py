import threading
import socket
import json
import select
import random
import time
from enum import Enum
from threading import Lock
from typing import Dict, Any, List, Tuple


class ConnectionStatus(Enum):
    SELECTING_LOBBY = 1
    IN_LOBBY = 2
    IN_GAME = 3
    GAME_ENDED = 4


NEW_STATUS = 'NEW'
SELECTING_LOBBY_STATUS = ConnectionStatus.SELECTING_LOBBY.name
IN_LOBBY_STATUS = ConnectionStatus.IN_LOBBY.name
IN_GAME_STATUS = ConnectionStatus.IN_GAME.name
GAME_ENDED_STATUS = ConnectionStatus.GAME_ENDED.name

STATUS_KEY = 'status'
ACTION_KEY = 'action'
PLAYER_ID_KEY = 'player_id'
LOBBY_ID_KEY = 'lobby_id'
GAME_ID_KEY = 'game_id'
MAX_PLAYERS_KEY = 'max_players'
LOBBIES_LIST_KEY = 'lobbies_list'
LOBBY_KEY = 'lobby'
IN_LOBBY_KEY = 'in_lobby'
IN_GAME_KEY = 'in_game'
BOARD_KEY = 'board'
GAME_KEY = 'game'
MOVE_POSITIONS_KEY = 'move_positions'
SCORE_KEY = 'score'
TIME_KEY = 'time'

REFRESH_LIST_ACTION = 'refresh_list'
CREATE_LOBBY_ACTION = 'create_lobby'
JOIN_LOBBY_ACTION = 'join_lobby'
LEAVE_LOBBY_ACTION = 'leave_lobby'
MAKE_MOVE_ACTION = 'make_move'

# maps from ID's to objects
lobbies: Dict[int, 'Lobby'] = {}
games: Dict[int, 'Game'] = {}
players: Dict[int, 'ConnectionThread'] = {}

# locks on modification of dicts
lobbies_lock = threading.Lock()
games_lock = threading.Lock()
players_lock = threading.Lock()


def get_nonfull_lobbies() -> List[Dict]:
    return [x.to_json_dict() for x in lobbies.values() if len(x.players) != x.max_players]


def current_time():
    return int(time.time())


class Game:
    game_id: int
    deck: List[Tuple[int, int, int, int]]
    board: List[Tuple[int, int, int, int]]
    players: List[int]
    sets: List[Tuple[int, int, int]]
    scores: Dict[int, int]

    def __init__(self):
        self.game_id = None
        self.deck = [(x, y, z, t) for x in range(3) for y in range(3) for z in range(3) for t in range(3)]
        random.shuffle(self.deck)
        self.board = self.deck[:12]
        self.deck = self.deck[12:]
        self.players = []
        self.lock = threading.Lock()
        self.sets = []
        self.scores = {}
        self.time_start = current_time()
        self.time_end = 0

        self.gen_sets()

    def gen_sets(self):
        self.sets.clear()
        for i in range(12):
            for j in range(i + 1, 12):
                for k in range(j + 1, 12):
                    flag = True
                    for ind in range(4):
                        if not (self.board[i][ind] == self.board[j][ind] == self.board[k][ind] or (
                                self.board[i][ind] != self.board[j][ind] and self.board[j][ind] != self.board[k][ind] and self.board[i][ind] != self.board[k][ind])):
                            flag = False
                    if flag:
                        self.sets.append((i, j, k))

    def make_move(self, i, j, k, player_id):
        with self.lock:
            if not (i, j, k) in self.sets:
                return

            print('good move')

            self.board[i] = self.deck[0]
            self.board[j] = self.deck[1]
            self.board[k] = self.deck[2]
            self.gen_sets()

            while len(self.sets) == 0:
                print('regen sets')
                random.shuffle(self.deck)
                self.board[i] = self.deck[0]
                self.board[j] = self.deck[1]
                self.board[k] = self.deck[2]
                self.gen_sets()

            print('sets:', self.sets)
            self.deck = self.deck[3:]

            self.scores[player_id] += 1

            if len(self.deck) == 0:
                self.time_end = current_time()
                with players_lock:
                    for pl in self.players:
                        players[pl].game_ended.set()
            else:
                with players_lock:
                    for pl in self.players:
                        print('set event')
                        print(pl)
                        players[pl].board_updated.set()

    def to_json_dict(self):
        res = {GAME_ID_KEY: self.game_id,
               IN_GAME_KEY: self.players,
               BOARD_KEY: dict(enumerate(self.board))}
        return res


class Lobby:
    players: List[int]
    max_players: int
    lobby_id: int
    game_id: int

    def __init__(self, lobby_id, creator, max_players=2):
        self.players = [creator]
        self.lock = threading.Lock()
        self.max_players = max_players
        self.lobby_id = lobby_id
        self.game_id = None

    def add_player(self, player):
        with self.lock:
            self.players.append(player)
            with players_lock:
                for pl in self.players:
                    players[pl].lobby_updated.set()

        if len(self.players) == self.max_players:
            self.start_game()

    def remove_player(self, player):
        with self.lock:
            if player in self.players:
                self.players.remove(player)
                for pl in self.players:
                    players[pl].lobby_updated.set()

    def start_game(self):
        with self.lock:
            game = Game()
            game.game_id = random.randint(0, 1000)
            game.players = self.players.copy()
            game.scores = {pl: 0 for pl in self.players}

            self.game_id = game.game_id
            with games_lock:
                games[game.game_id] = game

            with players_lock:
                for pl in self.players:
                    players[pl].game_started.set()

    def to_json_dict(self):
        res = {LOBBY_ID_KEY: self.lobby_id,
               MAX_PLAYERS_KEY: self.max_players,
               IN_LOBBY_KEY: self.players,
               GAME_ID_KEY: self.game_id}
        return res


class ConnectionThread(threading.Thread):
    game_id: int
    lobby_id: int
    player_id: int
    socket: socket

    HANDSHAKE_WAIT = 5

    def __init__(self, sock):
        threading.Thread.__init__(self)
        self.socket = sock
        self.player_id = None
        self.lobby_id = None
        self.game_id = None
        self.status = ConnectionStatus.SELECTING_LOBBY

        self.lobby_updated = threading.Event()
        self.game_started = threading.Event()
        self.board_updated = threading.Event()
        self.game_ended = threading.Event()

    def send_as_json(self, data):
        # TODO make sure everything is sent
        self.socket.send((json.dumps(data) + '\n').encode('utf-8'))

    def get_default_json(self):
        return {STATUS_KEY: self.status.name,
                PLAYER_ID_KEY: self.player_id,
                LOBBY_ID_KEY: self.lobby_id,
                GAME_ID_KEY: self.game_id}

    def process_selecting_lobby(self):
        # player can request to refresh list, connect to lobby or create new lobby
        # TODO

        # get request from user
        s = ""
        r, w, e = select.select([self.socket], [], [])
        while len(r) > 0:
            s += r[0].recv(1024).decode('utf-8')
            r, w, e = select.select([self.socket], [], [], 0)
        request = json.loads(s)

        # check everything is ok
        if request[PLAYER_ID_KEY] != self.player_id or request['status'] != SELECTING_LOBBY_STATUS:
            # something went wrong
            # reply with some error code
            # TODO
            self.socket.send(b'{"ok":false}')
            return

        if request[ACTION_KEY] == REFRESH_LIST_ACTION:
            ans = self.get_default_json()
            ans[LOBBIES_LIST_KEY] = get_nonfull_lobbies()
            self.send_as_json(ans)
        elif request[ACTION_KEY] == CREATE_LOBBY_ACTION:
            self.lobby_id = random.randint(0, 1000)
            with lobbies_lock:
                lobbies[self.lobby_id] = Lobby(self.lobby_id, self.player_id, request[MAX_PLAYERS_KEY])
            self.status = ConnectionStatus.IN_LOBBY

            ans = self.get_default_json()
            ans[LOBBY_KEY] = lobbies[self.lobby_id].to_json_dict()
            self.send_as_json(ans)
        elif request[ACTION_KEY] == JOIN_LOBBY_ACTION:
            self.lobby_id = request[LOBBY_ID_KEY]
            if len(lobbies[self.lobby_id].players) == lobbies[self.lobby_id].max_players:
                # cannot join lobby because it is full
                self.lobby_id = None
                ans = self.get_default_json()
                ans[LOBBIES_LIST_KEY] = get_nonfull_lobbies()
                ans['error_reason'] = 'Lobby is full'
                self.send_as_json(ans)
                return
            with lobbies_lock:
                lobbies[self.lobby_id].add_player(self.player_id)

            self.status = ConnectionStatus.IN_LOBBY
            ans = self.get_default_json()
            ans[LOBBY_KEY] = lobbies[self.lobby_id].to_json_dict()
            self.send_as_json(ans)

    def process_in_lobby(self):
        # player can leave lobby, or start game if he is creator
        # we need to send updates on players in lobby, reassign creator if previous leaves
        # and notify when game starts

        # something changed in lobby, notify user
        if self.lobby_updated.is_set():
            self.lobby_updated.clear()

            ans = self.get_default_json()
            ans[LOBBY_KEY] = lobbies[self.lobby_id].to_json_dict()
            self.send_as_json(ans)
            return

        if self.game_started.is_set():
            self.game_started.clear()

            self.game_id = lobbies[self.lobby_id].game_id
            self.status = ConnectionStatus.IN_GAME
            ans = self.get_default_json()
            ans[GAME_KEY] = games[self.game_id].to_json_dict()
            self.send_as_json(ans)
            return

        # TODO
        s = ""
        r, w, e = select.select([self.socket], [], [], 0)
        while len(r) > 0:
            s += r[0].recv(1024).decode('utf-8')
            r, w, e = select.select([self.socket], [], [], 0)

        # didn't get any request from user
        if len(s) == 0:
            return

        request = json.loads(s)

        # check everything is ok
        if request[PLAYER_ID_KEY] != self.player_id or request['status'] != IN_LOBBY_STATUS:
            # something went wrong
            # reply with some error code
            # TODO
            self.socket.send(b'{"ok":false}')
            return

        if request[ACTION_KEY] == LEAVE_LOBBY_ACTION:
            with lobbies_lock:
                lobbies[self.lobby_id].remove_player(self.player_id)
            self.status = ConnectionStatus.SELECTING_LOBBY
            self.lobby_id = None
            ans = self.get_default_json()
            ans[LOBBIES_LIST_KEY] = get_nonfull_lobbies()
            self.send_as_json(ans)

    def process_in_game(self):
        # player can leave game, make a move
        # we need to send updates on board condition

        if self.game_ended.is_set():
            # TODO send score
            self.game_ended.clear()

            self.status = ConnectionStatus.GAME_ENDED
            ans = self.get_default_json()
            ans[SCORE_KEY] = games[self.game_id].scores
            ans[TIME_KEY] = games[self.game_id].time_end - games[self.game_id].time_start
            self.send_as_json(ans)
            return

        if self.board_updated.is_set():
            self.board_updated.clear()

            ans = self.get_default_json()
            ans[GAME_KEY] = games[self.game_id].to_json_dict()
            self.send_as_json(ans)
            return

        s = ""
        r, w, e = select.select([self.socket], [], [], 0)
        while len(r) > 0:
            s += r[0].recv(1024).decode('utf-8')
            r, w, e = select.select([self.socket], [], [], 0)

        # didn't get any request from user
        if len(s) == 0:
            return

        print(s)
        request = json.loads(s)

        if request[ACTION_KEY] == MAKE_MOVE_ACTION:
            positions = request[MOVE_POSITIONS_KEY]
            games[self.game_id].make_move(positions[0], positions[1], positions[2], self.player_id)
        # TODO

    def run(self):
        # initial handshake
        # determine if player is new or is reconnecting
        s = ""
        r, w, e = select.select([self.socket], [], [], self.HANDSHAKE_WAIT)
        while len(r) > 0:
            s += r[0].recv(1024).decode('utf-8')
            r, w, e = select.select([self.socket], [], [], 0)
        print(s)
        handshake = json.loads(s)

        if handshake[STATUS_KEY] == NEW_STATUS:
            # give player an ID and return list of lobbies
            self.player_id = random.randint(0, 1000)
            with players_lock:
                players[self.player_id] = self

            self.status = ConnectionStatus.SELECTING_LOBBY
            ans = self.get_default_json()
            ans[LOBBIES_LIST_KEY] = get_nonfull_lobbies() 
            self.send_as_json(ans)
        elif handshake[STATUS_KEY] == SELECTING_LOBBY_STATUS:
            # player gives us ID
            # check that such player was connected and return list of lobbies
            # TODO
            pass
        elif handshake[STATUS_KEY] == IN_LOBBY_STATUS:
            # player gives us ID and lobby
            # check that player really is in that lobby and return lobby
            # TODO
            pass
        elif handshake[STATUS_KEY] == IN_GAME_STATUS:
            # player gives us ID and game
            # check that player really is in that game and return game
            # TODO
            pass

        while True:
            if self.status == ConnectionStatus.SELECTING_LOBBY:
                self.process_selecting_lobby()
            elif self.status == ConnectionStatus.IN_LOBBY:
                self.process_in_lobby()
            elif self.status == ConnectionStatus.IN_GAME:
                self.process_in_game()
        pass


if __name__ == "__main__":
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server.bind(('', 3691))
    server.listen(5)
    while True:
        client, address = server.accept()
        client.setblocking(False)
        print('got new connection')
        cthread = ConnectionThread(client)
        cthread.start()
